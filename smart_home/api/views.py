from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import TV, ACunit, Bath, ClothesWasher, DishWasher, Dryer, ExhaustFan, HotWaterHeater, LightBulb, Microwave, Oven, Stove, Thermostat, Room
from .serializers import ACunitSerializer, ThermostatSerializer, RoomSerializer, LightBulbSerializer, ExhaustFanSerializer, ExhaustFanSerializer, MicrowaveSerializer, HotWaterHeaterSerializer, StoveSerializer, OvenSerializer, TVSerializer, DryerSerializer, BathSerializer, DishWasherSerializer, ClothesWasherSerializer


@api_view(['GET'])
def getRoutes(request):

    routes = [
        {
            'Endpoint': '/notes/',
            'method': 'GET',
            'body': None,
            'description': 'Returns an array of Stuff'
        }
    ]

    return Response(routes)

#Thermostat Views
@api_view(['GET'])
def getThermostats(request):
    thermostats = Thermostat.objects.all()
    serializer = ThermostatSerializer(thermostats, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getThermostat(request, pk):
    thermostat = Thermostat.objects.get(id=pk)
    serializer = ThermostatSerializer(thermostat, many=True)
    return Response(serializer.data)


#Room Views
@api_view(['GET'])
def getRooms(request):
    thermostats = Room.objects.all()
    serializer = RoomSerializer(thermostats, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getRoom(request, pk):
    thermostat = Room.objects.get(id=pk)
    serializer = RoomSerializer(thermostat, many=True)
    return Response(serializer.data)

#AC Unit Views
@api_view(['GET'])
def getAcUnits(request):
    acUnits = ACunit.objects.all()
    serializer = ACunitSerializer(acUnits, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getAcUnit(request, pk):
    acUnit = ACunit.objects.get(id=pk)
    serializer = ACunitSerializer(acUnit, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateAcUnit(request,pk):
    data = request.data
    acUnit = ACunit.objects.get(id=pk)
    serializer = ACunitSerializer(instance=acUnit, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Light Bulb Views
@api_view(['GET'])
def getLightBulbs(request):
    lightBulbs = LightBulb.objects.all()
    serializer = LightBulbSerializer(lightBulbs, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getLightBulb(request, pk):
    lightBulb = LightBulb.objects.get(id=pk)
    serializer = LightBulbSerializer(lightBulb, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateLightBulb(request,pk):
    data = request.data
    lightBulb = LightBulb.objects.get(id=pk)
    serializer = LightBulbSerializer(instance=lightBulb, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#ExhaustFan Views
@api_view(['GET'])
def getExhaustFans(request):
    exhaustFans = ExhaustFan.objects.all()
    serializer = ExhaustFanSerializer(exhaustFans, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getExhaustFan(request, pk):
    exhaustFan = ExhaustFan.objects.get(id=pk)
    serializer = ExhaustFanSerializer(exhaustFan, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateExhaustFan(request,pk):
    data = request.data
    exhaustFan = ExhaustFan.objects.get(id=pk)
    serializer = ExhaustFanSerializer(instance=exhaustFan, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Microwave Views
@api_view(['GET'])
def getMicrowaves(request):
    microwaves = Microwave.objects.all()
    serializer = MicrowaveSerializer(microwaves, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getMicrowave(request, pk):
    microwave = Microwave.objects.get(id=pk)
    serializer = MicrowaveSerializer(microwave, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateMicrowave(request,pk):
    data = request.data
    microwave = Microwave.objects.get(id=pk)
    serializer = MicrowaveSerializer(instance=microwave, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Hot Water Heater Views
@api_view(['GET'])
def getHotWaterHeaters(request):
    hotWaterHeaters = HotWaterHeater.objects.all()
    serializer =  HotWaterHeaterSerializer(hotWaterHeaters, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getHotWaterHeater(request, pk):
    hotWaterHeater = HotWaterHeater.objects.get(id=pk)
    serializer = HotWaterHeaterSerializer(hotWaterHeater, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateHotWaterHeater(request,pk):
    data = request.data
    hotWaterHeater = HotWaterHeater.objects.get(id=pk)
    serializer = HotWaterHeaterSerializer(instance=hotWaterHeater, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Stove Views
@api_view(['GET'])
def getStoves(request):
    stoves = Stove.objects.all()
    serializer = StoveSerializer(stoves, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getStove(request, pk):
    stove = Stove.objects.get(id=pk)
    serializer = StoveSerializer(stove, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateStove(request,pk):
    data = request.data
    stove = Stove.objects.get(id=pk)
    serializer = StoveSerializer(instance=stove, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Oven Views
@api_view(['GET'])
def getOvens(request):
    ovens = Stove.objects.all()
    serializer = OvenSerializer(ovens, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getOven(request, pk):
    oven = Oven.objects.get(id=pk)
    serializer = OvenSerializer(oven, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateOven(request,pk):
    data = request.data
    oven = Oven.objects.get(id=pk)
    serializer = OvenSerializer(instance=oven, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#TV
@api_view(['GET'])
def getTVs(request):
    tvs = TV.objects.all()
    serializer = TVSerializer(tvs, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getTV(request, pk):
    tv = TV.objects.get(id=pk)
    serializer = TVSerializer(tv, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateTV(request,pk):
    data = request.data
    tv = TV.objects.get(id=pk)
    serializer = TVSerializer(instance=tv, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Dryer Views
@api_view(['GET'])
def getDryers(request):
    dryer = Dryer.objects.all()
    serializer = DryerSerializer(Dryer, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getDryer(request, pk):
    dryer = Dryer.objects.get(id=pk)
    serializer = DryerSerializer(Dryer, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateDryer(request,pk):
    data = request.data
    dryer = Dryer.objects.get(id=pk)
    serializer = DryerSerializer(instance=dryer, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Bath Views
@api_view(['GET'])
def getBaths(request):
    bath = Bath.objects.all()
    serializer = BathSerializer(bath, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getBath(request, pk):
    bath = Bath.objects.get(id=pk)
    serializer = BathSerializer(bath, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateBath(request,pk):
    data = request.data
    bath = Bath.objects.get(id=pk)
    serializer = BathSerializer(instance=bath, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Dish Washer Views
@api_view(['GET'])
def getDishWashers(request):
    dishWasher = DishWasher.objects.all()
    serializer = DishWasherSerializer(dishWasher, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getDishWasher(request, pk):
    dishWasher = DishWasher.objects.get(id=pk)
    serializer = DishWasherSerializer(dishWasher, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateDishWasher(request,pk):
    data = request.data
    dishWasher = DishWasher.objects.get(id=pk)
    serializer = DishWasherSerializer(instance=dishWasher, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#Clothes Washer Views
@api_view(['GET'])
def getClothesWashers(request):
    clothesWasher = ClothesWasher.objects.all()
    serializer = ClothesWasherSerializer(clothesWasher, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def getClothesWasher(request, pk):
    clothesWasher = ClothesWasher.objects.get(id=pk)
    serializer = ClothesWasherSerializer(clothesWasher, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def updateClothesWasher(request,pk):
    data = request.data
    clothesWasher = ClothesWasher.objects.get(id=pk)
    serializer = ClothesWasherSerializer(instance=clothesWasher, data=data)
    
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)